create database data1 default character set utf8;

USE data1;

create table user(
  id SERIAL primary key AUTO_INCREMENT,
  login_id varchar(255) UNIQUE NOT NULL,
  name varchar(255) NOT NULL,
  birth_date DATE NOT NULL,
  password varchar(255) NOT NULL,
  create_date DATETIME NOT NULL,
  update_date DATETIME NOT NULL
);

INSERT INTO user VALUES(
  1,
  'admin',
  '管理者',
  '1996-08-05',
  'password',
  cast(now()as datetime),
  cast(now()as datetime)
);
