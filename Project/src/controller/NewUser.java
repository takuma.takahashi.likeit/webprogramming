package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Shinki
 */
@WebServlet("/NewUser")
public class NewUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("Login");
		}else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/html3.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String ID = request.getParameter("ID");
		String PW = request.getParameter("PW");
		String PW2 = request.getParameter("PW2");
		String Name = request.getParameter("name");
		String age = request.getParameter("age");

		UserDao userDao = new UserDao();
		User user=userDao.kakunin(ID);

		if( !(PW.equals(PW2)) || user!=null || ID=="" || PW=="" || PW2=="" || Name=="" || age=="") {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("ID", ID);
			request.setAttribute("Name", Name);
			request.setAttribute("age", age);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/html3.jsp");
			dispatcher.forward(request, response);
			return;
		}




		userDao.shinki(ID,PW,Name,age);
		response.sendRedirect("UserList");



	}

}
