<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザ情報詳細参照</title>
		<link rel="stylesheet" href="css2.css">
	</head>
	<body>
		<header>
		<span id="y">${userInfo.name}さん</span>
		<a href="logout" id="rog">ログアウト</a>
		</header>
		<h1>ユーザ情報詳細参照</h1>
		<table class="aaa">
			<tr>
				<th><label>ログインID</label></th><th>${syousai.loginId}</th>
			</tr>
			<tr>
				<th><label>ユーザ名</label></th><th>${syousai.name}</th>
			</tr>
			<tr>
				<th><label>生年月日</label></th><th>${syousai.birthDate}</th>
			</tr>
			<tr>
				<th><label>登録日時</label></th><th>${syousai.createDate}</th>
			</tr>
			<tr>
				<th><label>更新日時</label></th><th>${syousai.updateDate}</th>
			</tr>
		</table>
		<p id="sample2">
			<a href="UserList">戻る</a>
		</p>
	</body>
</html>