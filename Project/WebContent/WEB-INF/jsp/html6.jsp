<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザ削除確認</title>
		<link rel="stylesheet" href="css2.css">
	</head>
	<body>
		<header>
			<span id="y">${userInfo.name}さん</span>
			<a href="logout" id="rog">ログアウト</a>
		</header>
		<h1>ユーザ削除確認</h1>
		<p id="sample2">
			ログインID : ${syousai.loginId}<br>
			<span id="sample2">を本当に削除してよろしいでしょうか。</span>
		</p>
		<br>
		<br>
		<p id="sample3"></p>
		<form action="UserList" method="get">
			<input id="submit_button" type="submit" value="キャンセル">
		</form>
		<form action="UserDelete?id=${syousai.loginId}" method="post">
				<input id="submit_button" type="submit" value="削除">
		</form>

	</body>
</html>
