<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザ情報更新</title>
		<link rel="stylesheet" href="css2.css">
	</head>
	<body>
		<header>
		<span id="y">${userInfo.name}さん</span>
		<a href="logout" id="rog">ログアウト</a>
		</header>
		<h1>ユーザ情報更新</h1>
		<form action="UserUpdate" method="post">
			<table class="aaa">
				<tr>
					<th><label>ログインID</label></th><th>${syousai.loginId}</th>
				</tr>
				<tr>
					<th><label>パスワード</label></th><th><input type="password" name="PW" size="25"></th>
				</tr>
				<tr>
					<th><label>パスワード確認</label></th><th><input type="password" name="PW" size="25"></th>
				</tr>
				<tr>
					<th><label>ユーザー名</label></th><th><input type="text" name="name" size="25" value=${syousai.name}></th>
				</tr>
				<tr>
					<th><label>生年月日</label></th><th><input type="text" name="age" size="25" value=${syousai.birthDate}></th>
				</tr>
			</table>
			<p id="sample3">
				<input id="submit_button" type="submit" value="更新" name="kousin">
			</p>
		</form>
		<br>
		<p id="sample2">
			<a href=UserList>戻る</a>
		</p>
	</body>
</html>