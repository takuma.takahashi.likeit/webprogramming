<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン画面</title>
		<link rel="stylesheet" href="css/css1.css">
	</head>
	<body>
		<h1>ログイン画面</h1>
		<c:if test="${errMsg != null}" >
			<div id="sample6">
		  		${errMsg}
			</div>
		</c:if>
		<br>
		<form action="Login" method="post">
			<p id="sample0">
				<label for="id"><span class="sample1">ログインID</span></label>
				<input type="text" name="loginId" size="20" id="id"><br>
				<label><span class="sample1">パスワード</span></label>
				<input type="password" name="password" size="20"><br>
				<br>
				<input id="submit_button" type="submit" value="ログイン" name="IDPW">
			</p>
		</form>
	</body>
</html>