<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザ一覧</title>
		<link rel="stylesheet" href="css/css2.css">
		<link href="css/bootstrap.min.css" rel="stylesheet">

	</head>
	<body>
		<header>
		<span id="y">${userInfo.name}さん</span>
		<a href="logout" id="rog">ログアウト</a>
		</header>
		<h1>ユーザ一覧</h1>
		<p id="sample1">
			<a href="NewUser">新規登録</a>
		</p>
		<form action="UserList" method="post">
			<table class="aaa">
				<tr>
					<th><label>ログインID</label></th><th><input type="text" name="login-id" size="37"></th>
				</tr>
				<tr style="height: 10px; "><th></th></tr>
				<tr>
					<th><label>ユーザー名</label></th><th><input type="text" name="user-name" size="37"></th>
				</tr>
				<tr style="height: 10px; "><th></th></tr>
				<tr>
					<th><label>生年月日</label></th>
					<th><input type="text" name="date-start" size="14" value="年/月/日">〜
						<input type="text" name="date-end" size="14" value="年/月/日"></th>
				</tr>
			</table>
			<p id="sample1">
				<input  id="submit_button" type="submit" value="検索">
	   		</p>
		</form>
		<hr>
		<br>
		<table border="1" class="bbb">
			<tr>
				<th>ログインID</th><th>ユーザ名</th><th>生年月日</th><th></th><th></th><th></th>
			</tr>
			<tbody>
            	<c:forEach var="user" items="${userList}" >
					<tr>
		              <td>${user.loginId}</td>
		              <td>${user.name}</td>
		              <td>${user.birthDate}</td>
		              <!-- TODO 未実装；ログインボタンの表示制御を行う -->
		              <td id="sample5"><a class="btn btn-primary" href="UserDetail?id=${user.id}">詳細</a></td>
					  <td id="sample5"><c:if test="${userInfo.loginId == 'admin' || userInfo.loginId==user.loginId}">
		              <a class="btn btn-success" href="UserUpdate?id=${user.id}">更新</a>
		              </c:if></td>
		              <td id="sample5"><c:if test="${userInfo.loginId == 'admin'}">
		              <a class="btn btn-danger" href ="UserDelete?id=${user.id} ">削除</a>
		              </c:if></td>
		            </tr>
            	</c:forEach>
            </tbody>
		</table>
	</body>
</html>