<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザ新規登録</title>
		<link rel="stylesheet" href="css/css2.css">
	</head>
	<body>
		<header>
		<span id="y">${userInfo.name}さん</span>
		<a href="logout" id="rog">ログアウト</a>
		</header>
		<h1>ユーザ新規登録</h1>
		<c:if test="${errMsg != null}" >
			<div id="sample6">
		  		${errMsg}
			</div>
		</c:if>
		<form action="NewUser" method="post">
			<table class="aaa">
				<tr>
					<th><label>ログインID</label></th><th><input type="text" name="ID" value="${ID}" size="25"></th>
				</tr>
				<tr>
					<th><label>パスワード</label></th><th><input type="password" name="PW" size="25"></th>
				</tr>
				<tr>
					<th><label>パスワード確認</label></th><th><input type="password" name="PW2" size="25"></th>
				</tr>
				<tr>
					<th><label>ユーザー名</label></th><th><input type="text" name="name" value="${Name}" size="25"></th>
				</tr>
				<tr>
					<th><label>生年月日</label></th><th><input type="text" name="age" value="${age}" size="25"></th>
				</tr>
			</table>
			<p id="sample3">
			<input id="submit_button" type="submit" value="登録">
			</p>
		</form>
		<br>
		<p id="sample2">
			<a href="UserList">戻る</a>
		</p>
	</body>
</html>